/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.storeproject.poc;

import Database.database;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author kittimasak
 */
public class TestDeleteProduct {
    public static void main(String[] args) throws SQLException {
        Connection conn = null;
        database db = database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "DELETE FROM product WHERE id=?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, 3);
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            System.out.println("Affect row " + row);
        } catch (SQLException ex) {
            Logger.getLogger(TestSelecProduct.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
    }
}
